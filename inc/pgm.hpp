#ifndef PGM_HPP
#define PGM_HPP

#include "imagem.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

class Pgm: public Imagem{
public: 
	Pgm();
	void descript(char * pixels,string comentario);
};

#endif