#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

class Imagem{
private: 
	string nomeImagem;
	string numMagico;
	string comentario;
	int largura, altura;
	int maximo;
	char * pixels;

public:
	Imagem();
	~Imagem();
	void setNomeImagem(string nomeImagem);
	string getNomeImagem();
	void setNumMagico(string numMagico);
	string getNumMagico();
	void setComentario(string comentario);
	string getComentario();
	void setLargura(int largura);
	int getLargura();
	void setAltura(int altura);
	int getAltura();
	void setMaximo(int maximo);
	int getMaximo();
	void setPixels(char * pixels);
	char * getPixels();
	int checarNumMagico(string numMagico);
	


	void lerImagem(string nomeImagem);
	//void salvarImagem();



};

#endif