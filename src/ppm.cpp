#include "ppm.hpp"
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
using namespace std;

Ppm::Ppm(){

}

void Ppm::lerImagem(string nomeImagem){

	ifstream arquivo;
	
	int largura, altura;
	int maximo, i;
	string numMagico, comentario, troca;
	stringstream lh;
	
	arquivo.open(nomeImagem);
	getline(arquivo, numMagico); //ler primeira linha e armazenar os dados
	setNumMagico(numMagico);
	getline(arquivo, comentario);
	setComentario(comentario);
	getline(arquivo, troca);
	lh.str(troca);
	lh >> largura >> altura;
	arquivo >> maximo;
	setAltura(altura);
	setLargura(largura);
	setMaximo(maximo);
	
	arquivo.get();
	char * pixels = new char[altura*largura*3]; //o 3 e pelo fato de a imgagem ter rgb
	setPixels(pixels);
	for(i = 0; i < altura*largura*3; i++){
		arquivo.get(pixels[i]); // armazenar cada char na ordenacao certa dentro da array
		}

	arquivo.close();

}


void Ppm::descript(char * pixels, string comentario){
	int firstPix, tam, i, j, somadorDePix;
	string alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ", firstC, chave, msg;
	stringstream dados;

	dados.str(comentario);
	dados >> firstC >> tam >> chave;
	for(j = 0; j < firstC.length(); j++){
		firstC[j] = firstC[j+1];
	}
	firstPix = stoi(firstC);

	//setar a chave para maiusculo
	for (i = 0; i < chave.length(); i++){
		chave[i] = toupper(chave[i]);
	}

	//checar se tem palavras repetidas na chave
	for (i = 0; i < chave.length(); i++){
		int pos = i;
		for (j = 0; j < chave.length(); j++){
			if (pos == j){
				continue;
			} else if(chave[i] == chave[j]){
				cout << "Existem palavras repetidas na chave encontrada. Erro fatal." << endl;
				exit(EXIT_FAILURE); // termina a execucao do programa em caso de erro
			}

		}
	}

	//gerar o alfabeto com a chave
	string keyAlpha = "";

	for (i = 0; i < chave.length(); i++){
		keyAlpha += chave[i];
	}
	for (i = 0; i < 26; i++){
		keyAlpha += (char) (i+65);
	} 

	//remover letras do alfabeto que ja estejam na chave e terminar o alfabeto
	for(i = 0; i < keyAlpha.length(); i++){
		bool repete = false;
		for (j = 0; j < chave.length(); j++){
			if(keyAlpha[i] == chave[j]){
				repete = true;
				break;
			}
		}
		if (repete == false){
			chave += keyAlpha[i];
		}
	}

	//encontrar a mensagem
	for (i = firstPix; i < (tam*3) + firstPix; i+=3){
		somadorDePix = (pixels[i] % 10) + (pixels[i+1] % 10) + (pixels[i+2] % 10);
		for (j = 0; j < 26; j++){
			if (somadorDePix == 0){
				msg += " ";
				break;
				
			} else if ((somadorDePix + '@') == chave[j]){
				msg += 'A' + j;
				break;
			}
		}

	}	

	cout << "A mensagem é: " << msg << endl;


}



