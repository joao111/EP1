#include "imagem.hpp"
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

Imagem::Imagem(){
	nomeImagem = "";
	numMagico = "";
	comentario = "";
	largura = 0;
	altura = 0;
	maximo = 0;
}
/*Imagem::Imagem(string nomeImagem){
	nomeImagem = "";
	numMagico = "";
	comentario = "";
	largura = 0;
	altura = 0;
	maximo = 0;
}*/
Imagem::~Imagem(){
	

}
void Imagem::setNomeImagem(string nomeImagem){
	this-> nomeImagem = nomeImagem;
}
string Imagem::getNomeImagem(){
	return nomeImagem;
}
void Imagem::setNumMagico(string numMagico){
	this-> numMagico = numMagico;
}
string Imagem::getNumMagico(){
	return numMagico;
}
void Imagem::setComentario(string comentario){
	this-> comentario = comentario;
}
string Imagem::getComentario(){
	return comentario;
}
void Imagem::setLargura(int largura){
	this-> largura = largura;
}
int Imagem::getLargura(){
	return largura;
}
void Imagem::setAltura(int altura){
	this-> altura = altura;
}
int Imagem::getAltura(){
	return altura;
}
void Imagem::setMaximo(int maximo){
	this-> maximo = maximo;
}
int Imagem::getMaximo(){
	return maximo;
}
void Imagem::setPixels(char * pixels){
	this-> pixels = pixels;
}
char * Imagem::getPixels(){
	return pixels;
}


void Imagem::lerImagem(string nomeImagem){
	

	ifstream arquivo;
	
	int largura, altura;
	int maximo, i, j;
	string numMagico, comentario, troca;
	stringstream lh;
	
	arquivo.open(nomeImagem);
	getline(arquivo, numMagico); //ler primeira linha e armazenar os dados
	setNumMagico(numMagico);
	getline(arquivo, comentario);
	setComentario(comentario);
	getline(arquivo, troca);
	lh.str(troca);
	lh >> largura >> altura;
	arquivo >> maximo;
	setAltura(altura);
	setLargura(largura);
	setMaximo(maximo);
	
	arquivo.get();
	char * pixels = new char[altura*largura];
	setPixels(pixels);
	for(i = 0; i < altura; i++){
		for(j = 0; j < largura; j++){
			arquivo.get(pixels[i*largura+j]); // armazenar cada char na ordenacao certa dentro da array

			
		}
		
	}

	arquivo.close();

	
}
	

/*int checarNumMagico(string numMagico){
	if (numMagico == "P2") {return 1};
	else if(numMagico == "P3") {return 0};
	else {
		cout << "Erro na verificação do tipo: número mágico inválido." << endl;


}*/