#include "imagem.hpp"
#include "pgm.hpp"
#include "ppm.hpp"
#include <iostream>

using namespace std;

int main (int argc, char ** argv){


	string nomeImagem, tipo;
	fstream i;
	cout << "Escreva o caminho/nome da sua Imagem: ";
	cin >> nomeImagem;
	i.open(nomeImagem);
	i >> tipo;
	i.close();

	if(tipo == "P5"){
		Pgm * imagem1 = new Pgm();

		imagem1->setNomeImagem(nomeImagem);
	
		imagem1->lerImagem(imagem1->getNomeImagem());
		imagem1->descript(imagem1->getPixels(), imagem1->getComentario());


	} else if (tipo == "P6"){
		Ppm * imagem1 = new Ppm();

		imagem1->setNomeImagem(nomeImagem);

		imagem1->lerImagem(imagem1->getNomeImagem());
		imagem1->descript(imagem1->getPixels(), imagem1->getComentario());

	} else {
		cout << "Erro ao criar um objeto" << endl;
	}


}
	