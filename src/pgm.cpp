#include "pgm.hpp"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>


Pgm::Pgm(){
	//setComentario("");
	//setPixels();
}

void Pgm::descript(char * pixels,string comentario){

	int chave, tam, firstPix, i, k, cont = 0;
	string firstC;
	
	stringstream dados;
	unsigned char ch; //cada char da mensagem que vai ser printada



	dados.str(comentario);
	dados >> firstC >> tam >> chave;

	for(k = 0; k < firstC.length(); k++){
		firstC[k] = firstC[k+1];
	}
	firstPix = stoi(firstC);

	cout << "A mensagem descriptografada encontrada é: "; //localizador
	
	for(i = firstPix; i < tam+firstPix; i++, cont++){
        ch = pixels[i];

        
        if(ch >= 'a' && ch <= 'z'){
            ch = ch - chave;
            
            if(ch < 'a'){
                ch = ch + 'z' - 'a' + 1;
            }
            
        }
        else if(ch >= 'A' && ch <= 'Z'){
            ch = ch - chave;
            
            if(ch > 'a'){
                ch = ch + 'Z' - 'A' + 1;
            }
            else if(ch >= '0' && ch <'A'){
                ch += 26;

            }           
        }
        
        cout << ch;
        
    }
    
    cout << endl;

}